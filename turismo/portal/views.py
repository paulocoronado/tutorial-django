from django.shortcuts import render
from django.http import HttpResponse
import datetime

def tiempo_view(request):
    now = datetime.datetime.now()
    html = "<html><body><h3>La hora en que se cargó está página es: %s.</h1></body></html>" % now
    return HttpResponse(html)
