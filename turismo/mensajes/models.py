from django.db import models

# Create your models here.

class SitioTuristico(models.Model):
    identificador =models.IntegerField()
    nombre        =models.TextField()
    descripcion   =models.TextField()
    latitud       =models.TextField()
    longitud      =models.DecimalField(max_digits=5, decimal_places=3)
    url           =models.CharField(max_length=100)
